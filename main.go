package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var onlyOnce sync.Once
var x []string

var dice = []int{1, 2, 3, 4, 5, 6}

func rollDice() int {
	onlyOnce.Do(func() {
		rand.Seed(time.Now().UnixNano())
	})

	return dice[rand.Intn(len(dice))]
}

type player struct {
	Nama  string
	Nilai int
	Dice  int
}

func resultDice() int {
	var nilai int
	dice := rollDice()
	if dice%2 == 0 {
		nilai = 5 //minus
	} else {
		nilai = 10 //plus
	}
	return nilai
}

func main() {
	var name string
	nilai := resultDice()
	dice := rollDice()

	fmt.Print("masukan nama kamu : ")
	fmt.Scanf("%s", &name)

	var tagList []player
	results := []player{player{Nama: name, Nilai: nilai, Dice: dice}}
	for _, list := range results {
		tagList = append(tagList, player{
			Nama: list.Nama, Nilai: list.Nilai, Dice: list.Dice})
	}

	fmt.Println("====================")
	fmt.Println("Nama Kamu : ", name)
	fmt.Println("Dice Kamu : ", dice)
	fmt.Println("Score Kamu : ", nilai)
	fmt.Println("listAll : ", tagList)
}
